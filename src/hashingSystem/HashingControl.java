package hashingSystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;

import hashingSystem.HashingControl;
import hashing.Hashing;
import hashingFrame.HashingFrame;

public class HashingControl {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}

	}
	
	public static void main (String[] args){
		// TODO Auto-generated method stub
		new HashingControl();
	}
	
	public HashingControl() {
		frame = new HashingFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(1000, 500);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}
	
	public void setTestCase(){
		Hashing h = new Hashing();
		Hashing h1 = new Hashing();
		Hashing h2 = new Hashing();
		Hashing h3 = new Hashing();
		String s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12;
		s1 = "https://www.facebook.com/";
		s2 = "https://www.edmodo.com/home#/backpack/";
		s3 = "https://www.youtube.com/";
	    s4 = "http://www.thaicreate.com/java/java-gui-swing-jtable.html";
		s5 = "https://bitbucket.org/5610450021/sc-lab-03/src";
		s6 = "https://www.google.co.th/?gws_rd=ssl";
		s7 = "http://beauty.wongnai.com/articles/the-most-beautiful-asian-women-in-the-world?ref=fbbeauty";
		s8 = "http://www.seriesubthai.com/";
		s9 = "http://javakoon.blogspot.com/2011/06/java-arraylist-arraylist-source-code.html";
		s10 = "http://mun2loads.blogspot.com/";
		s11 = "http://pantip.com/topic/33133437";
		s12 = "http://guru.google.co.th/guru/thread?tid=068037b012a3d15b";
		
		frame.setResult(s1 + "\t\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s1)) + "\n" +
						s2 + "\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s2)) + "\n" +
						s3 + "\t\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s3)) + "\n" +
						s4 + "\t\t\t Hashing is : " + h.modAscii(h.toAscii(s4)) + "\n" +
						s5 + "\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s5)) + "\n" +
						s6 + "\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s6)) + "\n" +
						s7 + "\t Hashing is : " + h.modAscii(h.toAscii(s7)) + "\n" +
						s8 + "\t\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s8)) + "\n" +
						s9 + "\t\t Hashing is : " + h.modAscii(h.toAscii(s9)) + "\n" +
						s10 + "\t\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s10)) + "\n" +
						s11 + "\t\t\t\t Hashing is : " + h.modAscii(h.toAscii(s11)) + "\n" +
						s12 + "\t\t\t Hashing is : " + h.modAscii(h.toAscii(s12)) + "\n" );
	}
	
	ActionListener list;
	HashingFrame frame;
}
